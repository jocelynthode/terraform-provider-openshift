package openshift

import (
	//"github.com/hashicorp/terraform/helper/resource"
	"github.com/hashicorp/terraform/helper/schema"
)

func resourceOpenshiftRoute() *schema.Resource {
	return &schema.Resource{
		Create: resourceOpenshiftRouteCreate,
		Read:   resourceOpenshiftRouteRead,
		Exists: resourceOpenshiftRouteExists,
		Update: resourceOpenshiftRouteUpdate,
		Delete: resourceOpenshiftRouteDelete,
		Importer: &schema.ResourceImporter{
			State: schema.ImportStatePassthrough,
		},

		Timeouts: &schema.ResourceTimeout{
			Create: schema.DefaultTimeout(10 * time.Minute),
			Update: schema.DefaultTimeout(10 * time.Minute),
			Delete: schema.DefaultTimeout(10 * time.Minute),
		},

		Schema: map[string]*schema.Schema{
			"metadata": namespacedMetadataSchema("route", true),

}
