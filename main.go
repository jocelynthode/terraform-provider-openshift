package main

import (
	"github.com/hashicorp/terraform/plugin"
	"gitlab.com/jocelynthode/openshift/openshift"
)

func main() {
	plugin.Serve(&plugin.ServeOpts{
		ProviderFunc: openshift.Provider})
}
